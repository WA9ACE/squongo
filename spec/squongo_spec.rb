# frozen_string_literal: true

RSpec.describe Squongo do
  it 'has a version number' do
    expect(Squongo::VERSION).not_to be nil
  end

  it 'can find by id' do
  end

  it 'can find by key' do
  end
end
